---
title: "Flower Data Linear Regression"
output:
  html_document:
    df_print: paged
date: "02/2018"
author: "Crista Moreno"
---

## Description

------------

This Rnotebook contains the notes and R code for inspecting data and determining if there is a strong linear relationship between the predictions and results. 

Please note that this data does not represent real data.

<span style="color:blue">This notebook was created using the *free software* environment for statistical computing and graphics R and the editor RStudio. Using free software is vital for data analysis because it gives the analyst or scientist the freedom to study the data without any kind of restrictions.</span>.


![](smallRlogo.png)


Copyright (C) 2018 Crista Moreno

    Flower Data Linear Regression is free software: you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.




```{r, out.width = "375px", fig.align='center', echo=FALSE}
knitr::include_graphics("rose-165819_1920.jpg")
```

Blue rose photo available from [pixabay.com](pixabay.com). 

### Load the following packages into the R session

---------------------------------------------------

<!-- #library(qcc) -->
<!-- library(stringr) #for manipulating strings (characters) -->
<!-- library(devtools) #to install packages from GitHub -->
<!-- library(ggthemes) #for different coloring themes of ggplot graphics -->

```{r, warning=FALSE, message=FALSE}
library(ggplot2)
library(plyr)
library(dplyr)
library(magrittr) #for piping commands
library(data.table)
library(scales)
library(curl)
library(reshape)  #to melt the data and plot multiple columns on one graph
library(lubridate)
library(coefplot) #for plotting coefficients of regression
library(ggpmisc) #for plotting regression line on ggplot
```


### Linear Regression and Coefficient of Determination

--------------------------------------------------------

Here we perform **simple linear regression** and compute the **coefficient of determination** 
$R^{2}$, where 

$R^{2} \equiv 1 - \dfrac{SS_{res}}{SS_{tot}}$

and 

$SS_{res} = \sum_{i}(y_{i} - f_{i})^{2} = \sum_{i}e_{i}^{2}$ (residual sum of squares)

$SS_{tot} = \sum_{i}(y_{i} - \bar{y})^{2}$ (total sum of sum of squares)

See the following link for references

[https://en.wikipedia.org/wiki/Coefficient_of_determination](https://en.wikipedia.org/wiki/Coefficient_of_determination)

One should take caution when using the $R^{2}$ measurement, for it is good for linear relationships (not necessairly non-linear relationships), is highly sensitive to single data points (outliers), and a good correlation with a high $R^{2}$ value does not imply a causation. 


### Read the Data into R 

-------------------------------------------

```{r}
# read the data into R as a data.frame
flower_data <- read.csv("flower_data.csv", header = TRUE)
```

### Inspect the Data

----------------------

```{r}
# display the first six rows of the data
flower_data %>% head()
```


```{r}
# print the data types of the different variables
flower_data$Property %>% class
flower_data$Origin %>% class
flower_data$Flower %>% class
flower_data$Prediction %>% class
flower_data$Result %>% class
flower_data$Planted %>% class
flower_data$Bloomed %>% class
```

### Change the Variables Planted and Bloomed to type Date

------------------------------------------------------------

```{r}
flower_data$Planted <- as.Date(flower_data$Planted, "%Y-%m-%d")
flower_data$Planted %>% class
```


```{r}
flower_data$Bloomed <- as.Date(flower_data$Bloomed, "%Y-%m-%d")
flower_data$Bloomed %>% class
```

### Create a Column for Year and Month

-----------------------------------------

```{r, warning=FALSE, message=FALSE}
year <- flower_data$Planted %>% year() %>% as.character()
month <- flower_data$Planted %>% month() %>% as.character()
month <- revalue(month, c("1"="01"))
year_month <- paste(year, month, sep = "/")
year_month_day <- paste(year_month, "01", sep="/")
flower_data$Year.Month <- ymd(year_month_day)
```

#### Create a Vector of Colors

-------------------------------

```{r, echo = FALSE}
colors <- c("darkorchid3", "magenta", "violet", "blue")
```

#### Function Missing Data 

---------------------------

```{r, warning=FALSE, message=FALSE, echo=TRUE}
missing_data_statistics <- function(data_temp) {
  
  data_size <- data_temp %>% nrow()
  print(paste(data_size, "size of the data"))
  
  results_missing <- data_temp %>% filter(is.na(Result)) %>% nrow()
  print(paste(results_missing, "number of results missing"))
  
  predictions_missing <- data_temp %>% filter(is.na(Prediction)) %>% nrow()
  print(paste(predictions_missing, "number of predictions missing"))
  
  print(paste(round(((results_missing / data_size) * 100), 2), "% Results Missing", sep = ""))
  print(paste(round(((predictions_missing / data_size) * 100), 2), "% Predictions Missing", sep = ""))
}
```

#### Print the Missing Data Statistics for Flower Data

---------------------------------------------------------

```{r}
missing_data_statistics(flower_data)
```


##### Create a Bar Plot of the Flower Data over Time

---------------------------------------------

```{r, fig.width=12, fig.height = 7,}
ggplot() + 
  geom_bar(data=na.omit(flower_data), aes(y = Prediction, x = Year.Month, fill=Flower), stat="identity") + 
  labs(title = "Chart faceted by Origin colored by Flower over Time") +
  
  scale_y_continuous(breaks = scales::pretty_breaks(n = 10)) + 
  scale_x_date(date_labels = "%b %Y", date_breaks = "4 month") +
  labs(xlab(label = "Month")) + 
  theme(axis.text.x = element_text(face="bold", size=9, angle=90),
        strip.text.x = element_text(size = 16, colour = "black", angle = 0)) + 
  facet_wrap(~Property)
```

### Flower Data Linear Regression `Prediction` verses `Result`

------------------------------------------------------------------

```{r, echo=TRUE, fig.width=10, fig.height = 5, warning=FALSE, message=FALSE}
data_temp <- flower_data %>% filter(Origin == "Garden1") %>% select(Result, Prediction, Property)

y <- data_temp$Result
x <- data_temp$Prediction

my.formula <- y~x

garden1_ggplot <- ggplot(data_temp, aes(x=Prediction, y=Result, color = colors[2])) + 
  
  geom_count(color = colors[2]) +
  
  scale_color_continuous(breaks = c(1, 10, 40, 80)) +
  scale_size_continuous(breaks = c(1, 10, 40, 80), range = c(3,18)) +
  
  scale_shape(solid = TRUE) + 
  scale_y_continuous(breaks = scales::pretty_breaks(n = 6)) + 
  scale_x_continuous(breaks = scales::pretty_breaks(n = 6)) +
  
  labs(title = "Garden 1 Linear Regression", 
       y = "Result", x = "Number of Flowers Prediction") + 
  
  geom_smooth(method = "lm", formula = my.formula, aes(color = y), 
              color = colors[2], se = TRUE, size = 1.5) + 
  geom_abline(mapping = NULL, data = NULL, slope = 1, intercept = 0,
              na.rm = FALSE, show.legend = TRUE) +
  facet_wrap(~Property)

garden1_ggplot + 
  stat_poly_eq(formula = my.formula, 
               aes(label = paste(..eq.label.., ..rr.label.., sep = "~~~")),
               color = colors[2], parse = TRUE, size = 6) + 
  theme(axis.text.x = element_text(face="bold", size=14, angle=0),
        axis.text.y = element_text(face="bold", size = 14), 
        axis.title.x = element_text(size=16), 
        axis.title.y = element_text(size=16),
        text=element_text(size=15),
        plot.title = element_text(size=20), 
        strip.text.x = element_text(size = 16, colour = "black", angle = 0)) 
```

Note that the black line represents the line $Result = Prediction$ and so any points that fall on this line means that the gardener correctly predicted the number of flowers that were going to bloom. 

If the point is above the line, then the gardener underestimated. 

### Save the Linear Regression Plots to a PNG file

-----------------------------------------------------

```{r, echo=TRUE, message=FALSE, warning=FALSE}
png(filename="Linear_Regression_cairo.png", 
    type="cairo",
    units="in", 
    width=12, 
    height=6, 
    pointsize=12, 
    res=350)

garden1_ggplot + 
  stat_poly_eq(formula = my.formula, 
               aes(label = paste(..eq.label.., ..rr.label.., sep = "~~~")),
               color = colors[2], parse = TRUE, size = 9) +
  theme(axis.text.x = element_text(face="bold", size=18, angle=0),
        axis.text.y = element_text(face="bold", size = 18), 
        axis.title.x = element_text(size=20), 
        axis.title.y = element_text(size=20),
        text=element_text(size=18),
        plot.title = element_text(size=22), 
        strip.text.x = element_text(size = 18, colour = "black", angle = 0)) 

dev.off()
```

### Plot the Linear Regression for `Garden3` data

---------------------------------------------------

```{r, echo=TRUE, fig.width=10, fig.height = 5, warning=FALSE, message=FALSE}
data_temp <- flower_data %>% filter(Origin == "Garden3") %>% select(Result, Prediction, Property)

y <- data_temp$Result
x <- data_temp$Prediction

my.formula <- y~x

garden2_ggplot <- ggplot(data_temp, aes(x=Prediction, y=Result, color = colors[3])) + 
  
  geom_count(color = colors[4]) +
  
  scale_color_continuous(breaks = c(1, 40, 80, 200)) +
  scale_size_continuous(breaks = c(1, 40, 80, 200), range = c(3,20)) +
  
  scale_shape(solid = TRUE) + 
  scale_y_continuous(breaks = scales::pretty_breaks(n = 6)) + 
  scale_x_continuous(breaks = scales::pretty_breaks(n = 6)) +
  
  labs(title = "Garden 3 Linear Regression", 
       y = "Result", x = "Number of Flowers Prediction") + 
  
  geom_smooth(method = "lm", formula = my.formula, aes(color = y), 
              color = colors[4], se = TRUE, size = 1.5) + 
  geom_abline(mapping = NULL, data = NULL, slope = 1, intercept = 0,
              na.rm = FALSE, show.legend = TRUE) +
  facet_wrap(~Property)

garden2_ggplot + 
  stat_poly_eq(formula = my.formula, 
               aes(label = paste(..eq.label.., ..rr.label.., sep = "~~~")),
               color = colors[4], parse = TRUE, size = 6) + 
  theme(axis.text.x = element_text(face="bold", size=14, angle=0),
        axis.text.y = element_text(face="bold", size = 14), 
        axis.title.x = element_text(size=16), 
        axis.title.y = element_text(size=16),
        text=element_text(size=15),
        plot.title = element_text(size=20), 
        strip.text.x = element_text(size = 16, colour = "black", angle = 0)) 
```
